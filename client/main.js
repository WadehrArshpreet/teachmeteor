
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import './main.html';
Template.body.helpers({
	resolutions:function(){
	if(Session.get('hideFinished')){
		return Resolutions.find({checked: {$ne: true}});
	}else{
			return Resolutions.find();
		}
	},
	hideFinished: function(){
		return Session.get('hideFinished');
	}
});
//insertion
Template.body.events({
	'submit .new-resolution' : function(event){
	var title = event.target.title.value;
	Resolutions.insert({
		name : title,
		CreatedAt : new Date()
	});

	event.target.title.value = '';
		return false;
	},

	'change .hide-finished' : function(event){
		Session.set('hideFinished', event.target.checked);
	}
});
//remove
Template.home.events({
	'click .toggle-checked' : function() {
		Resolutions.update(this._id, {$set: { checked: !this.checked }});
	},
	'click .delete' : function(){
		Resolutions.remove(this._id);
	}

});
Template.home.helpers({
	checked: function (id) {
		var ret;
		if(Resolutions.findOne({_id:id}).checked == undefined)
			ret = false;
		else
			ret = Resolutions.findOne({_id:id}).checked;
		return ret;
	}
});